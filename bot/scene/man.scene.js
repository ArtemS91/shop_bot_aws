const chooseMan = require("../keyboard/man.choosse.keyboard")
const sizeRaise = require("../keyboard/man/sizeRaise.keyboard");
const sizeSkate = require("../keyboard/man/sizeSkate.keyboard");
const sizeMax = require("../keyboard/man/sizeMax.keyboard");
const sizeCountry = require("../keyboard/man/sizeCountry.keyboard");
const sizeJordan = require("../keyboard/man/sizeJordan.keyboard");

const { Scenes, Composer, Markup } = require("telegraf");

const itemsManPhotos = new Composer();
itemsManPhotos.action('man', async (ctx) => {
  ctx.wizard.state.data = {};

  await ctx.replyWithMediaGroup([
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/men/ggpmsvx1odylrkceb8m7",
      caption:ctx.i18n.t('describeManItems.Raise')  },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/men/xgxfxncpwykoucdw4aan",
      caption:ctx.i18n.t('describeManItems.Skate') 
    },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/men/kxulujwjkjonvsw8xyfp",
      caption:ctx.i18n.t('describeManItems.Max') 
    },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/men/qkuhwpwenahyamxi5s65",
      caption:ctx.i18n.t('describeManItems.Country') 
    },
    {
        type: "photo",
        media:
          "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/men/lqckq9xzhwph1izfrgxb",
        caption:tx.i18n.t('describeManItems.Jordan') 
      },
  ],chooseMan);

  return ctx.wizard.next();
});
const itemsManSize = new Composer();
itemsManSize.on('text', async (ctx) => {
  ctx.wizard.state.data.item = ctx.message.text;
  if (ctx.message.text == "Raise") {
    console.log(ctx.session);
    ctx.wizard.state.data.price = "2000ua";
    await ctx.replyWithHTML(
ctx.i18n.t('manScema.Raise'),
sizeRaise
    );
  }
  else if (ctx.message.text == "Skate") {
    ctx.wizard.state.data.price = "2800ua";
    await ctx.replyWithHTML(
      ctx.i18n.t('manScema.Skate'),
      sizeSkate
    );
  }
  else if (ctx.message.text == "Max") {
    ctx.wizard.state.data.price = "3000ua";
    await ctx.replyWithHTML(
      ctx.i18n.t('manScema.Max'),
      sizeMax
    );
  }
  else if (ctx.message.text == "Country") {
    ctx.wizard.state.data.price = "3300ua";
    await ctx.replyWithHTML(
      ctx.i18n.t('manScema.Country'),
      sizeCountry
    );
  }
  else if (ctx.message.text == "Jordan") {
    ctx.wizard.state.data.price = "4000ua";
    await ctx.replyWithHTML(
      ctx.i18n.t('manScema.Jordan'),
      sizeJordan
    );
  }
  return ctx.wizard.next();
});


const getName = new Composer();
getName.on("text", async (ctx) => {
  ctx.wizard.state.data.size = ctx.message.text;
  await ctx.reply(`${ctx.i18n.t('schemaSame.Name')}`);
  return ctx.wizard.next();
});

const getNumber = new Composer();
getNumber.on("text", async (ctx) => {
  ctx.wizard.state.data.name = ctx.message.text;
  await ctx.reply(`${ctx.i18n.t('schemaSame.PhoneNumber')}`);
  return ctx.wizard.next();
});

const getPostDepartment = new Composer();
getPostDepartment.on("text", async (ctx) => {
  ctx.wizard.state.data.phone = ctx.message.text;
  await ctx.reply(`${ctx.i18n.t('schemaSame.PostDepartment')}`);
  return ctx.wizard.next();
});

const checkData = new Composer();
checkData.on("text", async (ctx) => {
  ctx.wizard.state.data.postDepartment = ctx.message.text;
  await ctx.reply(`${ctx.i18n.t('schemaSame.checkData')} ✅`);
  await ctx.replyWithHTML(`
<b>${ctx.i18n.t('schemaSame.orderInfo')} </b>
<b>${ctx.i18n.t('schemaSame.item')} </b>:   ${ctx.wizard.state.data.item}
<b>${ctx.i18n.t('schemaSame.size')} </b>:  ${ctx.wizard.state.data.size}
<b>${ctx.i18n.t('schemaSame.price')} </b>:    ${ctx.wizard.state.data.price}
<b>${ctx.i18n.t('schemaSame.clientsData')} </b> 
<b>${ctx.i18n.t('schemaSame.nameAndSername')} </b>:   ${ctx.wizard.state.data.name}
<b>${ctx.i18n.t('schemaSame.numberOfDepartment')} </b>: ${ctx.wizard.state.data.postDepartment} 
<b>${ctx.i18n.t('schemaSame.numberOfPhone')} </b>:   ${ctx.wizard.state.data.phone}
    `);
  await ctx.reply(`${ctx.i18n.t('schemaSame.checkInfo')} ?✍️`, Markup.inlineKeyboard([
    Markup.button.callback(`${ctx.i18n.t('keyboards.approveData')}`, 'approve'),
    Markup.button.callback(`${ctx.i18n.t('keyboards.reapproveData')}`, 'man'),
  
], {columns:1}));
  return await ctx.scene.leave();
});





const manWizard = new Scenes.WizardScene(
  "man-scene",
  itemsManPhotos,
  itemsManSize,
  getName,
  getNumber,
  getPostDepartment,
  checkData,
);

module.exports = manWizard;