const choose = require("../keyboard/woman.chosse.keyboard");
const sizeArchi = require("../keyboard/sizeArchi.keyboard");
const sizeSemmy =require("../keyboard/sizeSemmy.keyboard")
const sizeAngela =require("../keyboard/sizeAngela.keyboard")
const sizeFree =require("../keyboard/sizeFree.keyboard")
const { Scenes, Composer } = require("telegraf");


const itemsWomanPhotos = new Composer();
itemsWomanPhotos.action('woman', async (ctx) => {
  ctx.wizard.state.data = {};

  await ctx.replyWithMediaGroup([
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/woman/yxbfvsqj4f28iroyzrjd",
      caption:ctx.i18n.t('describeWomanItems.Archi')     },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/woman/i7yrxanzu2yd3vv3uce0",
      caption:ctx.i18n.t('describeWomanItems.Semmy')     },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/woman/zisw4ikeicjb1dcy6wr0",
      caption:ctx.i18n.t('describeWomanItems.Angela')     },
    {
      type: "photo",
      media:
        "https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/v1/woman/x68jrmsrlzztayzkssla",
      caption:ctx.i18n.t('describeWomanItems.Free')     },
  ],choose);


  return ctx.wizard.next();
});
const itemsWomanSize = new Composer();
itemsWomanSize.on('text', async (ctx) => {
  ctx.wizard.state.data.item = ctx.message.text;
  if (ctx.message.text == "Archi") {
    console.log(ctx.session);
    ctx.wizard.state.data.price = "2400ua";
    await ctx.replyWithHTML(
ctx.i18n.t('womanScema.Archi'),
      sizeArchi
    );
  }
  else if (ctx.message.text == "Semmy") {
    ctx.wizard.state.data.price = "2700ua";
    await ctx.replyWithHTML(
      ctx.i18n.t('womanScema.Semmy'),
      sizeSemmy
    );
  }
  else if (ctx.message.text == "Angela") {
    ctx.wizard.state.data.price = "2200ua";
    await ctx.replyWithHTML(
      ctx.i18n.t('womanScema.Angela'),
      sizeAngela
    );
  }
  else if (ctx.message.text == "Free") {
    ctx.wizard.state.data.price = "3000ua";
    await ctx.replyWithHTML(
      ctx.i18n.t('womanScema.Free'),
      sizeFree
    );
  }
  return ctx.wizard.next();
});

const getName = new Composer();
getName.on("text", async (ctx) => {
  ctx.wizard.state.data.size = ctx.message.text;
  await ctx.reply(`${ctx.i18n.t('schemaSame.Name')}`);
  return ctx.wizard.next();
});

const getNumber = new Composer();
getNumber.on("text", async (ctx) => {
  ctx.wizard.state.data.name = ctx.message.text;
  await ctx.reply(`${ctx.i18n.t('schemaSame.PhoneNumber')}`);
  return ctx.wizard.next();
});

const getPostDepartment = new Composer();
getPostDepartment.on("text", async (ctx) => {
  ctx.wizard.state.data.phone = ctx.message.text;
  await ctx.reply(`${ctx.i18n.t('schemaSame.PostDepartment')}`);
  return ctx.wizard.next();
});

const checkData = new Composer();
checkData.on("text", async (ctx) => {
  ctx.wizard.state.data.postDepartment = ctx.message.text;
  await ctx.reply(`${ctx.i18n.t('schemaSame.checkData')} ✅`);
  await ctx.replyWithHTML(`
<b>${ctx.i18n.t('schemaSame.orderInfo')} </b>
<b>${ctx.i18n.t('schemaSame.item')} </b>:   ${ctx.wizard.state.data.item}
<b>${ctx.i18n.t('schemaSame.size')} </b>:  ${ctx.wizard.state.data.size}
<b>${ctx.i18n.t('schemaSame.price')} </b>:    ${ctx.wizard.state.data.price}
<b>${ctx.i18n.t('schemaSame.clientsData')} </b> 
<b>${ctx.i18n.t('schemaSame.nameAndSername')} </b>:   ${ctx.wizard.state.data.name}
<b>${ctx.i18n.t('schemaSame.numberOfDepartment')} </b>: ${ctx.wizard.state.data.postDepartment} 
<b>${ctx.i18n.t('schemaSame.numberOfPhone')} </b>:   ${ctx.wizard.state.data.phone}
    `);
  await ctx.reply(`${ctx.i18n.t('schemaSame.checkInfo')} ?✍️`, Markup.inlineKeyboard([
    Markup.button.callback(`${ctx.i18n.t('keyboards.approveData')}`, 'approve'),
    Markup.button.callback(`${ctx.i18n.t('keyboards.reapproveData')}`, 'woman'),
  
], {columns:1}));
  return await ctx.scene.leave();
});


const womanWizard = new Scenes.WizardScene(
  "woman-scene",
  itemsWomanPhotos,
  itemsWomanSize,
  getName,
  getNumber,
  getPostDepartment,
  checkData,
);

module.exports = womanWizard;
