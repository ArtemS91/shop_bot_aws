const {session}=require('telegraf')
const TelegrafI18n = require("i18n-telegraf")
const path = require("path");
const bot=require('./token.bot');

const i18n = new TelegrafI18n({
  directory: path.resolve("./bot/locales"),
  defaultLanguage: 'ua',
  allowMissing: true,
  useSession: true,
  defaultLanguageOnMissing: true,
  });

  bot.use(session())
  bot.use(i18n.middleware())
 

  


