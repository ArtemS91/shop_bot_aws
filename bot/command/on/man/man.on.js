const bot =require('../../../connection/token.bot')
const choose=require('../../../keyboard/man.choosse.keyboard')
module.exports=bot.action('man',async (ctx)=> {
    await ctx.replyWithHTML(`${ctx.i18n.t('womanaAndmanOn.greetings')} <i>${ctx.chat.first_name}</i>.
    
⬇️${ctx.i18n.t('womanaAndmanOn.infoAboutProduct')}.⬇️`,choose);
    return await ctx.scene.enter('man-scene')
}
    )