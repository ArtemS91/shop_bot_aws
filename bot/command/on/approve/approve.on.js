const { Markup } = require('telegraf');
const bot=require('../../../connection/token.bot');
bot.action('approve',async(ctx)=>{
  await ctx.reply(`${ctx.i18n.t('keyboards.sendPayment')}`, Markup.inlineKeyboard([
    Markup.button.callback(`${ctx.i18n.t('keyboards.cardNow')}`, 'now'),
    Markup.button.callback(`${ctx.i18n.t('keyboards.later')}`, 'later'),
  
], {columns:1}))})