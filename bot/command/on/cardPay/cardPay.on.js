const { Markup } = require('telegraf')
const bot=require('../../../connection/token.bot')


bot.action('now', async(ctx)=>{
    await ctx.reply(`${ctx.i18n.t('approveData.payNow')}`,Markup.inlineKeyboard([
        Markup.button.url(`${ctx.i18n.t('keyboards.linkForPay')}`, 'https://secure.wayforpay.com/button/ba6faa42b68bb'),
    
      
    ], {columns:1}))
})