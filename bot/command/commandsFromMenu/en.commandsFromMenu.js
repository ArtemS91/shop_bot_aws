const bot=require('../../connection/token.bot')

bot.command('en',async(ctx)=>{
 ctx.i18n.locale('en')
 ctx.session.lang='en'
  const answer =ctx.i18n.t('change.english')
  return await ctx.reply(answer)
})