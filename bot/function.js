const { Markup } = require("telegraf");

module.exports=async(ctx)=>{
    try{
      console.log(ctx.session);

   if(!ctx.session){
    ctx.session={lang:'ua'}
          ctx.i18n.locale('ua')
        }
        else if(ctx.session.lang ==='ua'){
          ctx.i18n.locale('ua')
        }
        else{
          ctx.session.lang={lang:'en'}
          ctx.i18n.locale('en')}
   
  const description =ctx.i18n.t('start.description')
  const questing =ctx.i18n.t('start.questing')

   await ctx.replyWithMediaGroup([{
                  type:'photo',
                  media:'https://res.cloudinary.com/dllwyhvzg/image/upload/f_auto,q_auto/arwptwzdttghw7tkn2av',
                  caption:description,
                  caption_entities:description
              }])
  return  await ctx.reply(questing,Markup.inlineKeyboard([
    Markup.button.callback(`${ctx.i18n.t('start.woman')}`, 'woman'),
    Markup.button.callback(`${ctx.i18n.t('start.man')}`, 'man')
], {columns:2}))
      }
      catch(erro){
          console.log(erro);
      }
}